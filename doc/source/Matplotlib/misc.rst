Miscellaneous
*************


Annotation
==========

Annotation can be used to make graph more readable as show in :numref:`fig_annotateEx`. Text is added to graph using 'annotate()' command with two different methods as shown in line 25 and 40 in :numref:`py_matplotlib_annotateEx`. Also, 'text()' command (at line 49) is used to add text to the figure.



.. literalinclude:: codes/annotateEx.py
    :language: python
    :linenos:
    :caption: Annotation, :numref:`fig_annotateEx`
    :name: py_matplotlib_annotateEx


.. _`fig_annotateEx`:

.. figure:: figures/annotateEx.png
    :width: 80%
    :align: center
       
    Annotation, :numref:`py_matplotlib_annotateEx`


Sharing Axis
============


Listing  :numref:`py_matplotlib_shareaxisEx`, :numref:`py_matplotlib_shareaxisEx2` and  :numref:`py_matplotlib_legendPosEx` create the instances of figure() and subplot() functions of matplotlib to generate various plots. 


Common axis for two plots
-------------------------

Here x axis is common for two different plots. Further, in one plot log y-axis is used.
 

* Explanation :numref:`py_matplotlib_shareaxisEx`

    Line 11 creates an instance 'fig1' of figure() function. Then subfig1 and subfig2 instances of 'fig1' are created in line 14 and 15. 'twinx()' command in line 18, shares the x-axis for both the plots. 

    Line 22-24 set the various parameter for subfig1; also note that 'set\_' is used for x and y labels. Then line 27-28 plots the second figure. Finally line 30 displays both the plots as shown in :numref:`fig_shareaxisEx`


.. literalinclude:: codes/shareaxisEx.py
    :language: python
    :linenos:
    :caption: Sharing Axis, :numref:`fig_shareaxisEx`
    :name: py_matplotlib_shareaxisEx

.. _`fig_shareaxisEx`:
.. figure:: figures/shareaxisEx.png
    :width: 80%
    :align: center
       
    Shared x-axis by two figures, :numref:`py_matplotlib_shareaxisEx`



Sharing Axis-ticks
------------------


Here, same y-axis ticks (i.e. [-3, 2]) are used for two subplots as illustrated in :numref:`fig_shareaxisEx2` using :numref:`py_matplotlib_shareaxisEx2`. In the listing, line 15 and 16 create two subplots. Further, line 15 contains 'sharey' parameter which sets ticks in the y-axis of subfig2 equal to subfig1.  

.. literalinclude:: codes/shareaxisEx2.py
    :language: python
    :linenos:
    :caption: Sharing Y Axis ticks, :numref:`fig_shareaxisEx2`
    :name: py_matplotlib_shareaxisEx2

.. _`fig_shareaxisEx2`:

.. figure:: figures/shareaxisEx2.png
    :width: 80%
    :align: center
       
    Same Y axis values, :numref:`py_matplotlib_shareaxisEx2`


Add legend outside the plot
===========================

In :numref:`py_matplotlib_legendPosEx`, legends are placed outside the figure as shown in :numref:`fig_legendPosEx`. It can be quite useful, when we have large number of figures in a single plot. Note that, in line 12, instance of subplot is created directly; whereas in :numref:`fig_shareaxisEx2`, subplot are created using instances of figure(), which require 'add\_subplot' command as shown in line 14 and 15 there.

.. literalinclude:: codes/legendPosEx.py
    :language: python
    :linenos:
    :caption: Legend outside the plot, :numref:`fig_legendPosEx`
    :name: py_matplotlib_legendPosEx

.. _`fig_legendPosEx`:

.. figure:: figures/legendPosEx.png
    :width: 80%
    :align: center
       
    Legend outside the plot, :numref:`py_matplotlib_legendPosEx`