Basic plots
***********

Introduction
============

In this tutorial, Matplotlib library is discussed in detail, which is used for plotting the data. Our aim is to introduce the commonly used 'plot styles' and 'features' of the Matplotlib library, which are required for plotting the results obtained by the simulations or visualizing the data during machine learning process.



Data generation with Numpy
==========================

In this tutorial, Numpy library is used to generate the data for plotting. For this purpose, only 5 functions of numpy library are used, which are shown in :numref:`py_matplotlib_numpyDataEx`, 

.. literalinclude:: codes/numpyDataEx.py
        :language: python
        :linenos:
        :caption: Data generation using Numpy
        :name: py_matplotlib_numpyDataEx


Basic Plots
===========

In this section, basic elements of the plot e.g. Title, axis names and grid etc. are discussed. 

First Plot
----------

:numref:`py_matplotlib_firstPlot` plots the sin(x) as shown in Fig. :numref:`fig_firstPlot`.

* Explanation :numref:`py_matplotlib_firstPlot`

    Here, line 8 generates 100 equidistant points in the range :math:`[-2\pi, 2\pi]`. Then line 9 calculates the sine values for those points. Line 10 plots the figure, which is displayed on the screen using line 11. 

.. literalinclude:: codes/firstPlot.py
    :language: python
    :linenos:
    :caption: Sin(x), Fig. :numref:`fig_firstPlot`
    :name: py_matplotlib_firstPlot


.. _fig_firstPlot:

.. figure:: figures/firstPlot.png
    :width: 80%
    :align: center
       
    fig_firstPlot    


Label, Legend and Grid
----------------------


Here :numref:`py_matplotlib_firstPlot` is modified as shown in :numref:`py_matplotlib_firstPlotGL`, to add labels, legend and grid to the plot.

* Explanation :numref:`py_matplotlib_firstPlotGL`

    In line 11, label='sin' is added which is displayed by 'legend' command in line 15. 'loc=best' is optional parameter in line 15. This parameter find the best place for the legend i.e. the place where it does not touch the plotted curve. Line 18 and 19 add x and y label to curves. Finally, line 21 adds the grid-lines to the plot. 

    For changing the location of legend, replace 'best' in line 15 with 'center', 'center left', 'center right', 'lower center', 'lower left', 'lower right', 'right', 'upper center', 'upper left' or 'upper right'.

.. literalinclude:: codes/firstPlotGL.py
        :language: python
        :linenos:
        :caption: Grid, Label and Legend, :numref:`fig_firstPlotGAL`  
        :name: py_matplotlib_firstPlotGL


.. _fig_firstPlotGAL:

.. figure:: figures/firstPlotGL.png
    :width: 80%
    :align: center

    Grid, Axes, Label and Legend, :numref:`py_matplotlib_firstPlotGL`



Line style and Marker
---------------------

It is good to change the line styles and add markers to the plots with multiple graphs. It can be done as shown in :numref:`py_matplotlib_firstPlotLineMarker`.

* Explanation :numref:`py_matplotlib_firstPlotLineMarker`

    In line 13, '\*--r' is the combination of three separate parameters i.e. '\*', '--' and 'r', which represents 'marker', 'line style' and 'color' respectively. We can change the order of these combinations e.g. 'r--\*' and '--r\*' etc. Also, combination of two parameters (e.g. 'r--') or single parameter (e.g. '--') are valid. 

    Table :numref:`tbl_TableLineStyle`, :numref:`tbl_TableMarkerStyle` and :numref:`tbl_TableColorStyle` show some more abbreviations for 'line style', 'marker style' and 'color' respectively. Note that, only one element can be chosen from each style to make the combination. 

    Further, line 13 contains 'markersize' parameter for changing the size of the marker. Table :numref:`tbl_TablePlotStyle` shows the complete list of additional parameters to change the plot style. Lastly, line 13 can be rewritten using complete features of the plot as follows, 

    .. code-block:: python

        plt.plot(x, sinx, color='m',
            linestyle='-.', linewidth=4,
             marker='o', markerfacecolor='k', markeredgecolor='g',
             markeredgewidth=3, markersize=5,
            label='sin'
        )


.. literalinclude:: codes/firstPlotLineMarker.py
    :language: python
    :linenos:
    :caption: Line Style and Marker, :numref:`fig_firstPlotLineMarker`
    :name: py_matplotlib_firstPlotLineMarker


.. _fig_firstPlotLineMarker:

.. figure:: figures/firstPlotLineMarker.png
    :width: 80%
    :align: center
       
    Line-style and Line-marker, :numref:`py_matplotlib_firstPlotLineMarker`



.. _`tbl_TableLineStyle`:

.. table:: Line styles

    +---------+---------------+
    | Keyword | Description   |
    +=========+===============+
    | \-.     | dash-dot line |
    +---------+---------------+
    | --      | dashed line   |
    +---------+---------------+
    | \:      | dotted line   |
    +---------+---------------+
    | \-      | solid line    |
    +---------+---------------+

   

.. _`tbl_TableMarkerStyle`:

.. table:: Marker styles

    +---------+----------------+
    | Keyword | Description    |
    +=========+================+
    | o       | circle         |
    +---------+----------------+
    | x       | cross          |
    +---------+----------------+
    | D       | diamond        |
    +---------+----------------+
    | h       | hexagon        |
    +---------+----------------+
    | p       | pentagon       |
    +---------+----------------+
    | \+      | plus           |
    +---------+----------------+
    | .       | dot            |
    +---------+----------------+
    | s       | square         |
    +---------+----------------+
    | \*      | star           |
    +---------+----------------+
    | V       | down triangle  |
    +---------+----------------+
    | \<      | left triangle  |
    +---------+----------------+
    | \>      | right triangle |
    +---------+----------------+
    | \^      | up triangle    |
    +---------+----------------+




.. _`tbl_TableColorStyle`: 

.. table:: Color styles

     +---------+-------------+
     | Keyword | Description |
     +=========+=============+
     | k       | black       |
     +---------+-------------+
     | b       | blue        |
     +---------+-------------+
     | c       | cyan        |
     +---------+-------------+
     | g       | green       |
     +---------+-------------+
     | m       | magenta     |
     +---------+-------------+
     | r       | red         |
     +---------+-------------+
     | w       | white       |
     +---------+-------------+
     | y       | yellow      |
     +---------+-------------+



.. _`tbl_TablePlotStyle`: 

.. table:: Plot styles

    +-----------------+---------------------------+
    +=================+===========================+
    | color           | set the color of line     |
    +-----------------+---------------------------+
    | linestyle       | set the line style        |
    +-----------------+---------------------------+
    | linewidth       | set the line width        |
    +-----------------+---------------------------+
    | marker          | set the line-marker style |
    +-----------------+---------------------------+
    | markeredgecolor | set the marker edge color |
    +-----------------+---------------------------+
    | markeredgewidth | set the marker edge width |
    +-----------------+---------------------------+
    | markerfacecolor | set the marker face color |
    +-----------------+---------------------------+
    | markersize      | set the marker size       |
    +-----------------+---------------------------+

    

Axis and Title
--------------


:numref:`py_matplotlib_completeBasicEx` adds axis and title in previous figures. 

* Explanation :numref:`py_matplotlib_completeBasicEx`

    Lines 25 and 26 in the listing add display range for x and y axis respectively in the plot as shown in :numref:`fig_completeBasicEx`. Line 30 and 32 add the ticks on these axis. Further, line 31 adds various 'display-name' for the ticks. It changes the display of ticks e.g. 'np.pi' is normally displayed as '3.14', but  :math:`r'$+\backslash pi$` will display it as :math:`+\pi`. Note that :math:`\backslash pi` is the 'latex notation' for :math:`\pi`, and matplotlib supports latex notation as shown in various other examples as well in the tutorial.


.. literalinclude:: codes/completeBasicEx.py
    :language: python
    :linenos:
    :caption: Title and Axis, :numref:`fig_completeBasicEx`
    :name: py_matplotlib_completeBasicEx


.. _`fig_completeBasicEx`:

.. figure:: figures/completeBasicEx.png
    :width: 80%
    :align: center
       
    Axis-limit and Axis-marker, :numref:`py_matplotlib_completeBasicEx`



Multiple Plots
==============


In this section various mutliplots are discussed e.g. plots on the same figure window and subplots etc. 

Mutliplots in same window
-------------------------

By default, matplotlib plots all the graphs in the same window by overlapping the figures. In :numref:`py_matplotlib_multiplot`, line 14 and 15 generate two plots, which are displayed on the same figure window as shown in :numref:`fig_multiplot`.

.. literalinclude:: codes/multiplot.py
    :language: python
    :linenos:
    :caption: Mutliplots in same window, :numref:`fig_multiplot`
    :name: py_matplotlib_multiplot


.. _`fig_multiplot`:

.. figure:: figures/multiplot.png
    :width: 80%
    :align: center
       
    Multiplots, :numref:`py_matplotlib_multiplot`



Subplots
--------

In :numref:`fig_multiplot`, two plots are displayed in the same window. Further, we can divide the plot window in multiple sections for displaying each figure in different section as shown in :numref:`fig_subplotEx`

* Explanation :numref:`py_matplotlib_subplotEx`

    Subplot command takes three parameters i.e. number of rows, numbers of columns and location of the plot. For example in line 14, subplot(2,1,1),  divides the figure in 2 rows and 1 column, and uses location 1 (i.e. top) to display the figure. Similarly, in line 21, subplot(2,1,2) uses the location 2 (i.e. bottom) to plot the graph. Further, :numref:`py_matplotlib_histogramEx` divides the figure window in 4 parts and then location 1 (top-left), 2 (top-right), 3 (bottom-left) and 4 (bottom-right) are used to display the graphs. 

    Also, all the plot commands between line 14 and 21, e.g. line 15, will be displayed on the top location. Further, plots defined below line 21 will be displayed by bottom plot window.


.. literalinclude:: codes/subplotEx.py
    :language: python
    :linenos:
    :caption: Subplots, :numref:`fig_subplotEx`
    :name: py_matplotlib_subplotEx

.. _`fig_subplotEx`:

.. figure:: figures/subplotEx.png
    :width: 80%
    :align: center
       
    Subplots, :numref:`py_matplotlib_subplotEx`


Mutliplots in different windows
-------------------------------

'figure()' command is used to plot the graphs in different windows,  as shown in line 17 of :numref:`py_matplotlib_multiplotDifferentWindow`.

* Explanation :numref:`py_matplotlib_multiplotDifferentWindow`

    Here optional name 'Sin' is given to the plot which is displayed on the top in :numref:`fig_multiplotDifferentWindow_sin`. Then line 19 opens a new plot window with name 'Cos' as shown in :numref:`fig_multiplotDifferentWindow_cos`. Finally in line 21, the name 'Sin' is used again, which selects the previously open 'Sin' plot window and plot the figure there. Hence, we get two plots in this window (i.e. from lines 18 and 22) as show in :numref:`fig_multiplotDifferentWindow_sin`.


.. literalinclude:: codes/multiplotDifferentWindow.py
    :language: python
    :linenos:
    :caption: Mutliplots in different windows, :numref:`fig_multiplotDifferentWindow_sin` and :numref:`fig_multiplotDifferentWindow_cos`
    :name: py_matplotlib_multiplotDifferentWindow


.. _`fig_multiplotDifferentWindow_sin`:

.. figure:: figures/multiplotDifferentWindow.png
    :width: 80%
    :align: center
       
    Figure window with name 'Sin'

.. _`fig_multiplotDifferentWindow_cos`:

.. figure:: figures/multiplotDifferentWindow2.png
    :width: 80%
    :align: center
       
    Figure window with name 'Cos'