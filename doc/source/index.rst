.. Matplotlib guide documentation master file, created by
   sphinx-quickstart on Tue Nov 21 21:36:04 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Matplotlib Guide
================

.. toctree::
   :numbered:
   :maxdepth: 3
   :caption: Contents:

   Matplotlib/basic
   Matplotlib/types
   Matplotlib/misc


